# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.test import TestCase
from django_dynamic_fixture import G
from django.contrib.auth import get_user_model
from nzpg.apps.stream.models import Post
from .models import Like


User = get_user_model()

class LikeTestCase(TestCase):

    def test_total_given(self):
        me = G(User)
        user = G(User)
        self.assertEqual(Like.objects.total_given(me), 0)
        p = Post.objects.create(created_by=user)
        Like.objects.likes(me, p)
        self.assertEqual(Like.objects.total_given(me), 1)

    def test_total_given(self):
        me = G(User)
        user = G(User)
        self.assertEqual(Like.objects.total_received(user), 0)
        p = Post.objects.create(created_by=user)
        Like.objects.likes(me, p)
        self.assertEqual(Like.objects.total_received(user), 1)

    def test_top_liked_users(self):
        me = G(User)
        user = G(User)
        p = Post.objects.create(created_by=me)
        Like.objects.likes(user, p)
        self.assertEqual(Like.objects.top_liked_users()[0], me)
        self.assertEqual(Like.objects.top_liked_users()[0].liked_count, 1)
        user1 = G(User)
        Like.objects.likes(user1, p)
        self.assertEqual(Like.objects.top_liked_users()[0], me)
        self.assertEqual(Like.objects.top_liked_users()[0].liked_count, 2)
        p1 = Post.objects.create(created_by=me)
        Like.objects.likes(user, p)
        self.assertEqual(Like.objects.top_liked_users()[0], me)
        self.assertEqual(Like.objects.top_liked_users()[0].liked_count, 3)
        p2 = Post.objects.create(created_by=user)
        Like.objects.likes(me, p2)
        Like.objects.likes(user1, p2)
        self.assertEqual(Like.objects.top_liked_users()[1].liked_count, 2)


    def test_top_liked_users(self):
        me = G(User)
        user = G(User)
        p = Post.objects.create(created_by=me)
        Like.objects.likes(user, p)
        self.assertEqual(Like.objects.top_liked_content()[0], p)
        self.assertEqual(Like.objects.top_liked_content()[0].liked_count, 1)
        Like.objects.likes(me, p)
        self.assertEqual(Like.objects.top_liked_content()[0].liked_count, 2)
        p1 = Post.objects.create(created_by=me)
        Like.objects.likes(user, p1)
        self.assertEqual(Like.objects.top_liked_content()[1], p1)
        self.assertEqual(Like.objects.top_liked_content()[1].liked_count, 1)
