# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import RedirectView
from nzpg.apps.contrib.models import Trackable
from .models import Like
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from nzpg.apps.contrib.views import ActiveUserRequiredMixin
from braces.views import LoginRequiredMixin


# class ToggleLikeView(LoginRequiredMixin, RedirectView):
# get method doesn't need to be logged in
class ToggleLikeView(RedirectView):
    permanent = False

    def post(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return HttpResponse('Unauthorized', status=401)
        item = get_object_or_404(Trackable, uuid=self.kwargs.get('uuid'))
        action = 'like'
        user = self.request.user
        if Like.objects.liked(user, item):
            Like.objects.unlikes(user, item)
            action = 'unlike'
        else:
            Like.objects.likes(user, item)
        if request.is_ajax():
            return JsonResponse({'liked': Like.objects.liked(user, item), 'count': item.likes.count()})
        return super(ToggleLikeView, self).get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        item = get_object_or_404(Trackable, uuid=self.kwargs.get('uuid'))

        if request.is_ajax():
            return JsonResponse({'liked': Like.objects.liked(request.user, item) if request.user.is_authenticated() else False, 'count': item.likes.count()})
        return super(ToggleLikeView, self).get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return self.request.META.get('HTTP_REFERER') or '/'
