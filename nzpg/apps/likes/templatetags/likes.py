from django import template

register = template.Library()


@register.filter
def liked(content, user):
    try:
        if content.likes.filter(created_by=user).count():
            return True
    except:
        pass
    return False
