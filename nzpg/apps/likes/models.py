# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from nzpg.apps.contrib.models import Trackable
from nzpg.apps.contrib.managers import ContentManager
from django.db import models
from django.conf import settings
from .managers import LikeQuerySet


class Like(Trackable):
    item = models.ForeignKey(Trackable, related_name='likes')
    objects = ContentManager(LikeQuerySet)

    # http://stackoverflow.com/questions/28799949/django-polymorphic-models-with-unique-together?noredirect=1#comment45874235_28799949
    # class Meta:
        # unique_together = ['item', 'created_by']

    def get_absolute_url(self):
        return self.item.get_absolute_url()
