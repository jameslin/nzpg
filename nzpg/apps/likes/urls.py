from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^(?P<uuid>.+)/?$', views.ToggleLikeView.as_view(), name="toggle"),
)
