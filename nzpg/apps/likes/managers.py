# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db.models import Count
from django.contrib.auth import get_user_model
from nzpg.apps.contrib.models import Content
from polymorphic import PolymorphicQuerySet
import logging

log = logging.getLogger(__name__)


class LikeQuerySet(PolymorphicQuerySet):
    def likes(self, user, item, **kwarg):
        log.info("{} likes {}".format(user, item))
        return self.create(created_by=user, item=item, **kwarg)

    def liked(self, user, item):
        return self.filter(created_by=user, item=item).count() > 0

    def unlikes(self, user, item):
        log.info("{} unlikes {}".format(self, item))
        try:
            like = self.model.objects.get(item=item, created_by=user)
            like.delete()
            return True
        except self.model.DoesNotExist:
            pass
        return False

    def total_received(self, user):
        return self.filter(item__created_by=user).count()

    def total_given(self, user):
        return self.filter(created_by=user).count()

    def top_liked_users(self):
        User = get_user_model()
        return User.objects.annotate(liked_count=Count('trackable__likes')).order_by('-liked_count')

    def top_liked_content(self):
        return Content.objects.annotate(liked_count=Count('likes')).order_by('-liked_count')
