# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import floppyforms.__future__ as forms
from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['message']
