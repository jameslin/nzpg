# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from .forms import CommentForm


def comment_form(request):
    return {'comment_form': CommentForm()}
