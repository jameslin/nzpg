# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.contrib import admin
from .models import Comment
from nzpg.apps.contrib.admin import ContentAdminMixin


class CommentAdmin(ContentAdminMixin, admin.ModelAdmin):
    list_display = ['content', 'message', 'created_by', 'created_at']
    search_fields = ['created_by', 'content', 'message']


admin.site.register(Comment, CommentAdmin)
