# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from nzpg.apps.contrib.models import Content
from nzpg.apps.contrib.managers import ContentManager
from django.db import models
from .managers import CommentQuerySet


class Comment(Content):
    """
    Not sure if model inheritance is good combination with MPTT
    http://stackoverflow.com/questions/13018042/django-model-inheritance-using-mptt

    So in this case just vanilla self reference
    """
    content = models.ForeignKey(Content, related_name='comments')
    message = models.TextField()
    reply_to = models.ForeignKey('Comment', null=True, related_name='replies', blank=True)
    objects = ContentManager(CommentQuerySet)

    def add_reply(self, user, message):
        comment = self.content.add_comment(user, message)
        comment.reply_to = self
        comment.save()
        return comment

    def get_absolute_url(self):
        return self.content.get_absolute_url()
