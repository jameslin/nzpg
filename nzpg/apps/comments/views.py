# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import FormView
from nzpg.apps.contrib.models import Content
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from nzpg.apps.contrib.views import ActiveUserRequiredMixin
from .forms import CommentForm


class AddCommentView(ActiveUserRequiredMixin, FormView):
    form_class = CommentForm

    def form_valid(self, form):
        content = get_object_or_404(Content, uuid=self.kwargs.get('uuid'))
        comment = form.save(commit=False)
        comment.content = content
        comment.created_by = self.request.user
        comment.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self, *args, **kwargs):
        return self.request.META.get('HTTP_REFERER') or '/'
