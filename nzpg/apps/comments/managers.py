# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from nzpg.apps.contrib.managers import ContentQuerySet


class CommentQuerySet(ContentQuerySet):
    def make_comment(self, user, content, message, **kwarg):
        return self.create(created_by=user, content=content, message=message, **kwarg)
