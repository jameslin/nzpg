from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^add/(?P<uuid>.+)/?$', views.AddCommentView.as_view(), name="add"),
)
