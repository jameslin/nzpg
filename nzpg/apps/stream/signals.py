from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from nzpg.apps.photos.models import Album
from .models import Post
from open_facebook import OpenFacebook


@receiver(post_save, sender=Album)
def post_album_to_stream(sender, instance, created, *args, **kwargs):
    """
    Post to stream after creating an album
    """
    if created:
        description = render_to_string('stream/signals/album.html', {'album': instance})
        Post.objects.create(created_by=instance.created_by, description=description, mark_safe=True)


@receiver(post_save, sender=Post)
def post_to_facebook(sender, instance, created, *args, **kwargs):
    """
    Post to facebook after posted to stream
    """
    if created:
        #TODO remove return
        return
        try:
            description = render_to_string('stream/signals/facebook.txt', {'post': instance})
            facebook = OpenFacebook(instance.created_by.facebook_access_token)
            facebook.set('{}/feed'.format(settings.FACEBOOK_NZPG_ID), message=description)
        except:
            pass
