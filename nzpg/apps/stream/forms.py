import floppyforms.__future__ as forms
from .models import Post, PostPhoto


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['description', 'tags']


class PostPhotoForm(forms.ModelForm):
    class Meta:
        model = PostPhoto
        fields = ['description', 'photo']
