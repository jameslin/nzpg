# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.test import TestCase
from django_dynamic_fixture import G
from django.contrib.auth import get_user_model
from .models import Post


User = get_user_model()

class StreamTestCase(TestCase):

    def test_active_posts(self):
        user = G(User)
        p = Post.objects.create(created_by=user)
        self.assertTrue(p in Post.objects.stream_posts())
