from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.StreamView.as_view(), name="home"),
    url(r'^add/?$', views.AddPostView.as_view(), name="add"),
    url(r'^post/(?P<uuid>.+)/?$', views.PostView.as_view(), name="post"),
)
