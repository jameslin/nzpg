# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from nzpg.apps.contrib.managers import ContentQuerySet


class PostQuerySet(ContentQuerySet):
    def stream_posts(self):
        return self.active_content().filter(pinned=False)
