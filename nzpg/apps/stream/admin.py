# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.contrib import admin
from nzpg.apps.contrib.admin import ContentAdminMixin
from .models import Post


class PostAdmin(ContentAdminMixin, admin.ModelAdmin):
    pass

admin.site.register(Post, PostAdmin)
