# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db import models
from polymorphic import PolymorphicModel
from nzpg.apps.contrib.managers import ContentManager
from nzpg.apps.contrib.models import Content
from nzpg.apps.photos.models import Photo
from .managers import PostQuerySet
from django.core.urlresolvers import reverse_lazy


class Post(Content):
    description = models.TextField()
    objects = ContentManager(PostQuerySet)

    class Meta:
        ordering = ['-pk']

    def get_absolute_url(self):
        return reverse_lazy('stream:post', args=[self.uuid])

    def __unicode__(self):
        return u'Post: {}'.format(self.uuid)


class PostPhoto(Photo):
    post = models.ForeignKey(Post, related_name='photos')
