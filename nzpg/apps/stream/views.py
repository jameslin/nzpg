# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import ListView, DetailView
from extra_views import InlineFormSet
from .models import Post, PostPhoto
from .forms import PostForm, PostPhotoForm
from nzpg.apps.contrib.views import CreateContentWithInlineView, ActiveUserRequiredMixin
from django.core.urlresolvers import reverse_lazy
from braces.views import LoginRequiredMixin


class StreamView(ListView):
    model = Post
    paginate_by = 10
    template_name = 'stream/home.html'
    context_object_name = 'posts'

    def get_queryset(self):
        return Post.objects.stream_posts()

    def get_context_data(self, **kwargs):
        ctx = super(StreamView, self).get_context_data(**kwargs)
        ctx['pinned_posts'] = Post.objects.pinned_content()
        return ctx


class PostView(DetailView):
    model = Post
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    context_object_name = 'post'
    template_name = 'stream/post.html'


class PostPhotoInline(InlineFormSet):
    model = PostPhoto
    fk_name = 'post'
    form_class = PostPhotoForm
    extra = 5


class AddPostView(ActiveUserRequiredMixin, CreateContentWithInlineView):
    form_class = PostForm
    fk_name = 'post'
    model = Post
    inlines = [PostPhotoInline]
    template_name = 'stream/add_post.html'
    success_url = reverse_lazy('stream:home')
