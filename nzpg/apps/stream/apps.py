from django.apps.config import AppConfig


class StreamConfig(AppConfig):
    name = 'nzpg.apps.stream'
    verbose_name = 'Stream'

    def ready(self):
        import signals
