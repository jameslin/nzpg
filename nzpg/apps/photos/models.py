# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import os
from uuid import uuid4
from django.db import models
from django.utils.timezone import now
from nzpg.apps.contrib.models import Content
from datetime import datetime
from sorl.thumbnail import ImageField
from django.core.urlresolvers import reverse_lazy
from jsonfield import JSONField
import logging
import exifread

log = logging.getLogger(__name__)


def get_photo_filename(instance, filename):
    return datetime.strftime(now(), 'photo/%Y/%m/%d/{}.{}'.format(str(uuid4()), os.path.splitext(filename)[1]))


class Photo(Content):
    title = models.CharField(max_length=255, blank=True)
    photo = ImageField(upload_to=get_photo_filename)
    description = models.TextField(blank=True)
    exif_data = JSONField()

    def get_absolute_url(self):
        return reverse_lazy('photos:photo', args=[self.uuid])

    @property
    def exif(self):
        if self.exif_data == {}:
            try:
                for key, value in exifread.process_file(self.photo).items():
		    try:
			self.exif_data[key] = unicode(value)
		    except: pass
                if not self.exif_data:
                    self.exif_data = False
                self.save()
            except Exception as e:
		log.error(e)
                self.exif_data = False
        return self.exif_data


class Album(Content):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def get_absolute_url(self):
        return reverse_lazy('photos:album', args=[self.uuid])


class AlbumPhoto(Photo):
    album = models.ForeignKey(Album, related_name='photos')
