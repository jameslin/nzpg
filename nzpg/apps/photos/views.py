# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView, UpdateView, FormView
from extra_views import InlineFormSet
from nzpg.apps.contrib.views import CreateContentWithInlineView, ActiveUserRequiredMixin
from .models import Photo, Album, AlbumPhoto
from .forms import AlbumForm, AlbumPhotoForm, PhotoForm, PhotoUpdateForm
from django.core.urlresolvers import reverse_lazy


class AlbumListView(ListView):
    model = Album
    paginate_by = 10
    template_name = 'photos/home.html'
    context_object_name = 'albums'


class AlbumView(DetailView):
    model = Album
    template_name = 'photos/album.html'
    context_object_name = 'album'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'


class PhotoView(DetailView):
    model = Photo
    template_name = 'photos/photo.html'
    context_object_name = 'photo'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'


class PhotoInline(InlineFormSet):
    form_class = AlbumPhotoForm
    model = AlbumPhoto
    fk_name = 'album'


class AddAlbumView(ActiveUserRequiredMixin, CreateContentWithInlineView):
    form_class = AlbumForm
    model = Album
    inlines = [PhotoInline]
    fk_name = 'album'
    extra = 5
    template_name = 'photos/add_album.html'
    success_url = reverse_lazy('photos:home')


class PhotoBaseAddView(ActiveUserRequiredMixin, FormView):
    model = Photo
    form_class = PhotoForm


class PhotoBaseUpdateView(UpdateView):
    model = Photo
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    context_object_name = 'photo'
    form_class = PhotoUpdateForm

    def form_valid(self, form):
        # only saves the form if it's the owner
        if self.object.created_by == self.request.user:
            return super(PhotoBaseUpdateView, self).form_valid(form)
        return HttpResponseRedirect(self.get_success_url())
