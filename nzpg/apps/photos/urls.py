from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.AlbumListView.as_view(), name="home"),
    url(r'^add/?$', views.AddAlbumView.as_view(), name="add"),
    url(r'^album/(?P<uuid>.+)/?$', views.AlbumView.as_view(), name="album"),
    url(r'^photo/(?P<uuid>.+)/?$', views.PhotoView.as_view(), name="photo"),
)
