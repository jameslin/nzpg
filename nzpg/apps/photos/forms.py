# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import floppyforms.__future__ as forms
from .models import Album, AlbumPhoto, Photo
from nzpg.apps.photos.validators import max_photo_size


class PhotoForm(forms.ModelForm):
    photo = forms.ImageField(help_text="2MB Max", validators=[max_photo_size])

    class Meta:
        model = Photo
        fields = ['title', 'photo', 'description']


class PhotoUpdateForm(forms.ModelForm):
    photo = forms.ImageField(help_text="2MB Max", validators=[max_photo_size], required=False)


class AlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = ['title', 'description', 'tags']


class AlbumPhotoForm(forms.ModelForm):
    class Meta:
        model = AlbumPhoto
        fields = ['title', 'description', 'photo']
