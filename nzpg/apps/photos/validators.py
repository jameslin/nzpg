from django.core.exceptions import ValidationError
from django.conf import settings

def max_photo_size(value):
    if value._size > settings.MAX_PHOTO_SIZE:
        raise ValidationError('Your photo exceeded maximum 2MB file size.')
