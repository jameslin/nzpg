from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from . import views

urlpatterns = patterns('',
    url(r'^$', views.HomeView.as_view(), name="home"),
    url(r'^privacy/?$', TemplateView.as_view(template_name='home/privacy.html'), name="privacy"),
    url(r'^terms/?$', TemplateView.as_view(template_name='home/terms.html'), name="terms"),
    url(r'^contact/?$', TemplateView.as_view(template_name='home/contact.html'), name="contact"),
    url(r'^about/?$', TemplateView.as_view(template_name='home/about.html'), name="about"),
)
