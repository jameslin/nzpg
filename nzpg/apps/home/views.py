# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.shortcuts import render
from django.views.generic import RedirectView


class HomeView(RedirectView):
    permanent = False
    pattern_name = 'competitions:home'
