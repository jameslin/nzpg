# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import TemplateView, DetailView, FormView, UpdateView
from nzpg.apps.contrib.views import ActiveUserRequiredMixin, BaseDeleteView
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.paginator import Paginator
from .models import Competition, CompetitionPhoto
from .forms import CompetitionForm, CompetitionPhotoForm, CompetitionUpdateForm, CompetitionPhotoUpdateForm, CompetitionAdminForm, CompetitionAdminUpdateForm
from .exceptions import MaxPhotoReachedException
from nzpg.apps.photos.views import PhotoView, PhotoBaseAddView, PhotoBaseUpdateView
from nzpg.apps.votes.views import VoteMixin
import logging

log = logging.getLogger(__name__)


class CompetitionsView(TemplateView):
    template_name = 'competitions/home.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(CompetitionsView, self).get_context_data(*args, **kwargs)
        ctx['competitions'] = Competition.objects.all()[:10]
        return ctx


class CompetitionView(DetailView):
    template_name = 'competitions/competition.html'
    model = Competition
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    context_object_name = 'competition'

    def get_context_data(self, *args, **kwargs):
        ctx = super(CompetitionView, self).get_context_data(*args, **kwargs)

        # pagination
        paginator = Paginator(self.object.photos.all(), 10)
        paged = paginator.page(int(self.kwargs.get('page', 1)))
        ctx['paged'] = paged
        if self.object.in_entry_period and self.object.get_photos_left(self.request.user):
            ctx['can_entry'] = True
        ctx['winners'] = self.object.winners
        return ctx


class CompetitionUpdateView(UpdateView):
    template_name = 'competitions/competition_update.html'
    model = Competition
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    context_object_name = 'competition'
    form_class = CompetitionUpdateForm

    def form_valid(self, form):
        # only saves the form if it's the owner
        if self.object.created_by == self.request.user:
            return super(CompetitionUpdateView, self).form_valid(form)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('competitions:competition', args=[self.object.uuid])

    def get_form_class(self):
        print self.request.user.is_staff, 1111111
        if self.request.user.is_staff:
            return CompetitionAdminUpdateForm

        return super(CompetitionUpdateView, self).get_form_class()


class CompetitionAddView(ActiveUserRequiredMixin, FormView):
    model = Competition
    form_class = CompetitionForm
    template_name = 'competitions/competition_add.html'

    def form_valid(self, form):
        self.competition = form.save(commit=False)
        self.competition.created_by = self.request.user
        self.competition.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('competitions:competition', args=[self.competition.uuid])

    def get_form_class(self):
        if self.request.user.is_staff:
            return CompetitionAdminForm

        return super(CompetitionAddView, self).get_form_class()


class CompetitionPhotoAddView(PhotoBaseAddView):
    model = CompetitionPhoto
    form_class = CompetitionPhotoForm
    template_name = 'competitions/competition_photo_add.html'

    def get_success_url(self):
        return self.get_competition().get_absolute_url()

    def get_competition(self):
        return Competition.objects.get(uuid=self.kwargs.get('uuid'))

    def get_context_data(self, *args, **kwargs):
        ctx = super(CompetitionPhotoAddView, self).get_context_data(*args, **kwargs)
        ctx['competition'] = self.get_competition()
        return ctx

    def form_valid(self, form):
        competition = self.get_competition()
        if competition.get_photos_left(self.request.user):
            self.photo = form.save(commit=False)
            self.photo.created_by = self.request.user
            self.photo.competition = Competition.objects.get(uuid=self.kwargs.get('uuid'))
            self.photo.save()
        else:
            raise MaxPhotoReachedException()
        return HttpResponseRedirect(self.get_success_url())


class CompetitionPhotoUpdateView(PhotoBaseUpdateView):
    template_name = 'competitions/competition_photo_update.html'
    model = CompetitionPhoto
    form_class = CompetitionPhotoUpdateForm

    def get_success_url(self):
        return reverse('competitions:photo', args=[self.object.uuid])


class CompetitionPhotoView(VoteMixin, PhotoView):
    model = CompetitionPhoto
    template_name = 'competitions/competition_photo.html'

    def post(self, request, *args, **kwargs):
        """
        Vote
        """
        if 'vote' in request.POST:
            item = self.get_object()
            result = self.toggle_vote(self.request.user, item)
            if request.is_ajax():
                return JsonResponse({'count': item.votes.count(), 'voted': result})
        return super(CompetitionPhotoView, self).get(request, *args, **kwargs)

    def get_vote_item(self):
        return self.get_object()

    def get_context_data(self, *args, **kwargs):
        ctx = super(CompetitionPhotoView, self).get_context_data(*args, **kwargs)
        if self.request.user.is_authenticated():
            ctx['votes_left'] = self.object.competition.get_votes_left(self.request.user)
        return ctx


class CompetitionDeleteView(BaseDeleteView):
    success_url = reverse_lazy('competitions:home')


class CompetitionPhotoDeleteView(BaseDeleteView):
    def get_success_url(self):
        return reverse('competitions:competition', args=[self.object.competition.uuid])
