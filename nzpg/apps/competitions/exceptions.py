# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import


class CompetitionException(Exception):
    pass


class MaxVotesReachedException(CompetitionException):
    message = 'You cannot vote any more photos in this competition'

    def __init__(self, message=None):
        super(MaxVotesReachedException, self).__init__(message or self.message)


class MaxPhotoReachedException(CompetitionException):
    message = 'You cannot upload any more photos in this competition'

    def __init__(self, message=None):
        super(MaxPhotoReachedException, self).__init__(message or self.message)
