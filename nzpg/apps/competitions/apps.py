from django.apps.config import AppConfig


class CompetitionsConfig(AppConfig):
    name = 'nzpg.apps.competitions'
    verbose_name = 'Competitions'

    def ready(self):
        import signals
