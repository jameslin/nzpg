# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.test import TestCase
from django_dynamic_fixture import G
from django.contrib.auth import get_user_model
from nzpg.apps.competitions.models import Competition, CompetitionPhoto, Event
from nzpg.apps.votes.models import Vote
from django.utils.timezone import now
from datetime import timedelta
from .exceptions import MaxVotesReachedException

User = get_user_model()


class CompetitionTestCase(TestCase):

    def test_votes_limit(self):
        creator = G(User)
        competition = Competition.objects.create(entry_start_date=now(), entry_end_date=now(), vote_start_date=now(), vote_end_date=now(), created_by=creator, votes_per_user=2)
        photo1 = CompetitionPhoto.objects.create(competition=competition, created_by=creator)
        photo2 = CompetitionPhoto.objects.create(competition=competition, created_by=creator)
        photo3 = CompetitionPhoto.objects.create(competition=competition, created_by=creator)
        user = G(User)
        self.assertEqual(competition.get_votes_left(user), 2)
        photo1.voted_by(user)
        self.assertEqual(competition.get_votes_left(user), 1)
        photo2.voted_by(user)
        self.assertEqual(competition.get_votes_left(user), 0)
        with self.assertRaises(MaxVotesReachedException):
            photo2.voted_by(user)

    def test_events_tomorrow(self):
        tomorrow = now() + timedelta(days=1)
        day_after = tomorrow + timedelta(days=1)
        event = Event.objects.create(title='test', start_date=tomorrow, end_date=day_after, created_by=G(User))
        self.assertEqual(Event.objects.tomorrow().count(), 1)
        self.assertEqual(Event.objects.tomorrow()[0], event)
        event = Event.objects.create(title='test', start_date=day_after, end_date=day_after, created_by=G(User))
        self.assertEqual(Event.objects.tomorrow().count(), 1)

    def test_winners(self):
        creator = G(User)
        competition = Competition.objects.create(entry_start_date=now(), entry_end_date=now(), vote_start_date=now(), vote_end_date=now()+timedelta(days=1), created_by=creator, votes_per_user=2)
        photo1 = CompetitionPhoto.objects.create(competition=competition, created_by=creator)
        photo2 = CompetitionPhoto.objects.create(competition=competition, created_by=creator)
        photo3 = CompetitionPhoto.objects.create(competition=competition, created_by=creator)
        user = G(User)
        photo1.voted_by(user)
        self.assertEqual(competition.winners, [])
        competition.vote_end_date = now() + timedelta(days=-1)
        competition.save()
        self.assertEqual(competition.winners[0], photo1)
        photo1 = CompetitionPhoto.objects.get(id=photo1.id)
        self.assertTrue(photo1.is_winner)
