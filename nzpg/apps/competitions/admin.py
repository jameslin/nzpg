# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.contrib import admin
from nzpg.apps.contrib.admin import ContentAdminMixin
from .models import Competition


class CompetitionAdmin(ContentAdminMixin, admin.ModelAdmin):
    list_display = ['created_by', 'created_at', 'title']
    search_fields = ['title', 'created_by', 'description']

admin.site.register(Competition, CompetitionAdmin)
