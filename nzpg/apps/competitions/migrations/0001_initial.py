# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import nzpg.apps.photos.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contrib', '__first__'),
        ('photos', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Competition',
            fields=[
                ('content_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='contrib.Content')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('photo', models.ImageField(upload_to=nzpg.apps.photos.models.get_photo_filename)),
                ('entry_start_date', models.DateTimeField()),
                ('entry_end_date', models.DateTimeField()),
                ('vote_start_date', models.DateTimeField()),
                ('vote_end_date', models.DateTimeField()),
                ('photos_per_user', models.IntegerField(default=1)),
                ('votes_per_user', models.IntegerField(default=1)),
            ],
            options={
                'ordering': ['-pk'],
            },
            bases=('contrib.content',),
        ),
        migrations.CreateModel(
            name='CompetitionPhoto',
            fields=[
                ('photo_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='photos.Photo')),
                ('competition', models.ForeignKey(related_name='photos', to='competitions.Competition')),
            ],
            options={
                'ordering': ['-pk'],
            },
            bases=('photos.photo',),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('content_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='contrib.Content')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('location', models.TextField()),
                ('going', models.ManyToManyField(related_name='events_going', to=settings.AUTH_USER_MODEL)),
                ('maybe', models.ManyToManyField(related_name='events_maybe', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=('contrib.content',),
        ),
    ]
