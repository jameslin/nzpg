# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('competitions', '0002_auto_20150810_2120'),
    ]

    operations = [
        migrations.AddField(
            model_name='competitionphoto',
            name='is_winner',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
