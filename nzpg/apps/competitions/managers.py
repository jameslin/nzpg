# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from datetime import timedelta
from django.utils.timezone import now
from nzpg.apps.contrib.managers import ContentQuerySet


class CompetitionQuerySet(ContentQuerySet):
    def during_entry(self):
        current_time = now()
        return self.active_content().filter(entry_start_date__lte=current_time, entry_end_date__gte=current_time)

    def during_vote(self):
        current_time = now()
        return self.active_content().filter(vote_start_date__lte=current_time, vote_end_date__gte=current_time)

    def finished(self):
        return self.active_content().filter(vote_end_date__lte=now())


class EventQuerySet(ContentQuerySet):
    def coming(self):
        current_time = now()
        return self.active_content().filter(start_date__gte=current_time)

    def tomorrow(self):
        current_time = now()
        return self.coming().filter(start_date__gte=current_time, start_date__lte=current_time+timedelta(days=1))

    def past(self):
        current_time = now()
        return self.active_content().filter(end_date__lte=current_time)
