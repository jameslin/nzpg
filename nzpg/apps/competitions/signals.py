from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from nzpg.apps.competitions.models import Competition, CompetitionPhoto
from open_facebook import OpenFacebook
import logging

log = logging.getLogger(__name__)


@receiver(post_save, sender=Competition)
def post_competition_to_facebook(sender, instance, created, *args, **kwargs):
    """
    Post to facebook after posted to competition
    """
    if created:
        try:
            description = render_to_string('competitions/signals/facebook_competition.txt', {'competition': instance})
            facebook = OpenFacebook(instance.created_by.facebook_access_token)
            facebook.set('{}/feed'.format(settings.FACEBOOK_NZPG_ID), message=description)
        except Exception as e:
            log.error(e)


@receiver(post_save, sender=CompetitionPhoto)
def post_photo_to_facebook(sender, instance, created, *args, **kwargs):
    """
    Post to facebook after posted to competition photo
    """
    if created:
        try:
            description = render_to_string('competitions/signals/facebook_photo.txt', {'photo': instance})
            facebook = OpenFacebook(instance.created_by.facebook_access_token)
            facebook.set('{}/feed'.format(settings.FACEBOOK_NZPG_ID), message=description)
        except Exception as e:
            log.error(e)
