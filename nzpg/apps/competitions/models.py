# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db import models
from django.core.urlresolvers import reverse_lazy
from django.conf import settings
from django.utils.timezone import now
from django.db.models import Count, Max
from nzpg.apps.contrib.models import Content
from nzpg.apps.contrib.managers import ContentManager
from nzpg.apps.votes.models import Vote
from nzpg.apps.photos.models import Photo, get_photo_filename
from .managers import CompetitionQuerySet, EventQuerySet
from .exceptions import MaxVotesReachedException


class Competition(Content):
    title = models.CharField(max_length=255)
    description = models.TextField()
    photo = models.ImageField(upload_to=get_photo_filename)
    entry_start_date = models.DateTimeField()
    entry_end_date = models.DateTimeField()
    vote_start_date = models.DateTimeField()
    vote_end_date = models.DateTimeField()
    photos_per_user = models.IntegerField(default=1)
    votes_per_user = models.IntegerField(default=1)
    prize = models.TextField(blank=True)
    sponsor_content = models.TextField(blank=True)
    objects = ContentManager(CompetitionQuerySet)

    class Meta:
        ordering = ['-pk']

    @property
    def in_entry_period(self):
        return self.entry_start_date <= now() <= self.entry_end_date

    @property
    def in_vote_period(self):
        return self.vote_start_date <= now() <= self.vote_end_date

    @property
    def is_closed(self):
        return now() > self.vote_end_date

    def get_votes_left(self, user):
        """ number of likes left can be used in this competition """
        return self.votes_per_user - Vote.objects.filter(item__id__in=[photo.id for photo in self.photos.all()], created_by=user).count()

    def get_photos_left(self, user):
        if user.is_anonymous():
            return 0
        return self.photos_per_user - self.photos.filter(created_by=user).count()

    def get_absolute_url(self):
        return reverse_lazy('competitions:competition', args=[self.uuid])

    def leader_board(self):
        return sorted(self.photos.all(), key=lambda photo: photo.votes.count(), reverse=True)

    def __unicode__(self):
        return self.title

    @property
    def winners(self):
        """
        returns a list of top voted photos
        also sets the photo.is_winner attribute so we can query them
        """
        if not self.is_closed:
            return []

        photos = self.photos.filter(is_winner=True)
        # if we haven't set the winners we should try to set them
        if photos.count() == 0:
            top_votes = self.photos.annotate(total_votes=Count('votes')).aggregate(Max('total_votes'))['total_votes__max']
            # only populates the winners if there are votes
            if top_votes > 0:
                photos = self.photos.annotate(total_votes=Count('votes')).filter(total_votes=top_votes)
                photos.update(is_winner=True)
        return photos


class CompetitionPhoto(Photo):
    competition = models.ForeignKey(Competition, related_name='photos')
    is_winner = models.BooleanField(default=False)

    def voted_by(self, user):
        if not self.competition.get_votes_left(user):
            raise MaxVotesReachedException()
        Vote.objects.vote(user, self)

    def next_photo(self):
        try:
            return self.competition.photos.filter(pk__lt=self.pk)[0]
        except:
            return self.competition.photos.first()

    def previous_photo(self):
        try:
            return self.competition.photos.filter(pk__gt=self.pk).order_by('pk')[0]
        except:
            return self.competition.photos.all().order_by('pk')[0]

    def get_absolute_url(self):
        return reverse_lazy('competitions:photo', args=[self.uuid])

    def __str__(self):
        return self.title or 'unnamed competition photo'

    class Meta:
        ordering = ['-pk']


class Event(Content):
    title = models.CharField(max_length=255)
    description = models.TextField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    location = models.TextField()
    going = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="events_going")
    maybe = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="events_maybe")
    objects = ContentManager(EventQuerySet)
