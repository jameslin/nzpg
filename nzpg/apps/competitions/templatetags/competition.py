# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django import template

register = template.Library()


@register.filter
def get_photos_left(competition, user):
    return competition.get_photos_left(user)
