from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^/?$', views.CompetitionsView.as_view(), name="home"),
    url(r'^add/?$', views.CompetitionAddView.as_view(), name="add"),
    url(r'^photo/(?P<uuid>.+)/update/?$', views.CompetitionPhotoUpdateView.as_view(), name="update_photo"),
    url(r'^photo/(?P<uuid>.+)/delete/?$', views.CompetitionPhotoDeleteView.as_view(), name="delete_photo"),
    url(r'^photo/(?P<uuid>.+)/?$', views.CompetitionPhotoView.as_view(), name="photo"),
    url(r'^(?P<uuid>.+)/add-photo/?$', views.CompetitionPhotoAddView.as_view(), name="add_photo"),
    url(r'^(?P<uuid>.+)/update/?$', views.CompetitionUpdateView.as_view(), name="update"),
    url(r'^(?P<uuid>.+)/delete/?$', views.CompetitionDeleteView.as_view(), name="delete"),
    url(r'^(?P<uuid>.+)/(?P<page>[1-9]+)/?$', views.CompetitionView.as_view(), name="competition"),
    url(r'^(?P<uuid>.+)/?$', views.CompetitionView.as_view(), name="competition"),
)
