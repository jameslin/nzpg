import floppyforms.__future__ as forms
from .models import Competition, CompetitionPhoto, Event
from nzpg.apps.photos.validators import max_photo_size


class CompetitionForm(forms.ModelForm):
    photo = forms.ImageField(help_text="2MB Max", validators=[max_photo_size])

    class Meta:
        model = Competition
        fields = ['title', 'description', 'photo', 'entry_start_date', 'entry_end_date', 'vote_start_date', 'vote_end_date', 'photos_per_user', 'votes_per_user', 'prize']
        localized_fields = ('__all__',)
        widgets = {
            'entry_start_date': forms.TextInput(attrs={'placeholder': 'DD/MM/YYYY [optional: HH:MM]'}),
            'entry_end_date': forms.TextInput(attrs={'placeholder': 'DD/MM/YYYY [optional: HH:MM]'}),
            'vote_start_date': forms.TextInput(attrs={'placeholder': 'DD/MM/YYYY [optional: HH:MM]'}),
            'vote_end_date': forms.TextInput(attrs={'placeholder': 'DD/MM/YYYY [optional: HH:MM]'}),
        }


class CompetitionUpdateForm(CompetitionForm):
    photo = forms.ImageField(help_text="2MB Max", validators=[max_photo_size], required=False)


class CompetitionAdminForm(CompetitionForm):
    class Meta(CompetitionForm.Meta):
        fields = ['title', 'description', 'photo', 'entry_start_date', 'entry_end_date', 'vote_start_date', 'vote_end_date', 'photos_per_user', 'votes_per_user', 'prize', 'sponsor_content']


class CompetitionAdminUpdateForm(CompetitionUpdateForm):
    class Meta(CompetitionUpdateForm.Meta):
        fields = ['title', 'description', 'photo', 'entry_start_date', 'entry_end_date', 'vote_start_date', 'vote_end_date', 'photos_per_user', 'votes_per_user', 'prize', 'sponsor_content']


class CompetitionPhotoForm(forms.ModelForm):
    photo = forms.ImageField(help_text="2MB Max", validators=[max_photo_size])

    class Meta:
        model = CompetitionPhoto
        fields = ['title', 'photo', 'description']


class CompetitionPhotoUpdateForm(forms.ModelForm):

    class Meta:
        model = CompetitionPhoto
        fields = ['title', 'description']


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['title', 'description', 'start_date', 'end_date', 'location']
        localized_fields = '__all__'
