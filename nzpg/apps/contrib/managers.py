# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from polymorphic import PolymorphicQuerySet, PolymorphicManager


class ContentQuerySet(PolymorphicQuerySet):
    def inactive_content(self):
        return self.filter(status='inactive')

    def active_content(self):
        return self.filter(status='active')

    def pinned_content(self):
        return self.filter(status='active', pinned=True)


class ContentManager(PolymorphicManager):
    # for related managers
    use_for_related_fields = True
    queryset_class = ContentQuerySet
