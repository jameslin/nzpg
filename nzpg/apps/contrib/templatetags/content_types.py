# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django import template

register = template.Library()


@register.filter
def classname(obj):
    classname = obj.__class__.__name__
    return classname
