# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from extra_views import CreateWithInlinesView
from django.http import HttpResponseRedirect
from django.views.generic import DeleteView
from braces.views import LoginRequiredMixin, UserPassesTestMixin
from .models import Trackable


class CreateContentWithInlineView(CreateWithInlinesView):

    def forms_valid(self, form, inlines):
        """
        If the form and formsets are valid, save the associated models.
        """
        self.object = form.save(commit=False)
        self.object.created_by = self.request.user
        self.object.save()
        form.save_m2m()
        for formset in inlines:
            instances = formset.save(commit=False)
            for instance in instances:
                instance.created_by = self.request.user
                instance.save()
        return HttpResponseRedirect(self.get_success_url())


class ActiveUserRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    raise_exception = True

    def test_func(self, user):
        return user.can_post


class IsOwnMixin(object):
    def get_object(self, *args, **kwargs):
        obj = super(IsOwnMixin, self).get_object(*args, **kwargs)
        if not obj.created_by == self.request.user:
            raise Http404
        return obj


class BaseDeleteView(DeleteView, IsOwnMixin):
    model = Trackable
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
