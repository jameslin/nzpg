# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf import settings
from django.db import models
from django.utils.safestring import mark_safe
from polymorphic import PolymorphicModel
from django_extensions.db.fields import UUIDField
from taggit.managers import TaggableManager
from .managers import ContentManager


class Trackable(PolymorphicModel):
    uuid = UUIDField(unique=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Trackable'


class Content(Trackable):
    STATUS_CHOICES = (
        ('active', 'Active'),
        ('inactive', 'Inactive'),
    )

    status = models.CharField(choices=STATUS_CHOICES, default='active', max_length=25)
    tags = TaggableManager(blank=True)
    pinned = models.BooleanField(default=False)
    mark_safe = models.BooleanField(default=False)
    objects = ContentManager()

    def deactivate(self):
        self.status = 'inactive'
        self.save()

    def activate(self):
        self.status = 'active'
        self.save()

    @property
    def is_active(self):
        return self.status == 'active'

    @property
    def safe_description(self):
        if self.mark_safe:
            return mark_safe(self.description)
        return self.description
