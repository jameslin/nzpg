# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin
from .models import Trackable, Content


class TrackableAdminMixin(object):
    readonly_fields = ['created_by']
    list_display = ['created_by', 'created_at']
    search_fields = ['created_by']
    date_hierachy = 'created_at'

class TrackableAdmin(TrackableAdminMixin, PolymorphicChildModelAdmin):
    # polymorphic_list = True
    base_model = Trackable


class ContentAdminMixin(object):
    readonly_fields = ['created_by', 'tags']
    list_display = ['created_by', 'created_at']
    search_fields = ['created_by', 'description']
    save_on_top = True
    preserve_filters = True
    list_filter = ['status', 'pinned']
    date_hierachy = 'created_at'

class ContentAdmin(ContentAdminMixin, TrackableAdmin):
    polymorphic_list = True
    base_model = Content

admin.site.register(Trackable)
admin.site.register(Content)
