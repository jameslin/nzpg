from django.apps.config import AppConfig


class UserConfig(AppConfig):
    name = 'nzpg.apps.users'
    verbose_name = 'User'

    def ready(self):
        import signals
