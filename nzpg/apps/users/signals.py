# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth import logout
from allauth.account.signals import user_signed_up, user_logged_in


@receiver(user_logged_in)
@receiver(user_signed_up)
def sync_profile_picture(request, user, *args, **kwargs):
    user.profile_picture = user.facebook.get('{}/picture'.format(user.facebook_account.uid), redirect=False)['data']['url']
    # user.is_active = user.in_facebook_group
    user.save()
