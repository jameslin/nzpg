# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django import template

register = template.Library()


@register.simple_tag
def user_href(user):
    return user.get_absolute_url()


@register.simple_tag
def user_link(user):
    return '<a href="{}">{}</a>'.format(user.get_absolute_url(), user)
