# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from allauth.account.adapter import DefaultAccountAdapter
from allauth.exceptions import ImmediateHttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


class NZPGAccountAdapter(DefaultAccountAdapter):
    def login(self, request, user):
        from django.contrib.auth import login
        # HACK: This is not nice. The proper Django way is to use an
        # authentication backend
        if not hasattr(user, 'backend'):
            user.backend \
                = "allauth.account.auth_backends.AuthenticationBackend"

        user.is_active = user.in_facebook_group
        user.save()

        if not user.is_active:
            raise ImmediateHttpResponse(HttpResponseRedirect(reverse('users:login_fail')))
        login(request, user)
