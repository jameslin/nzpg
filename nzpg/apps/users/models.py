# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import

# Import the AbstractUser model
from django.contrib.auth.models import AbstractUser

# Import the basic Django ORM models library
from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.utils.functional import cached_property
from django_extensions.db.fields import UUIDField
from django.conf import settings
from django.core.urlresolvers import reverse
from allauth.socialaccount.models import SocialToken, SocialAccount
from open_facebook import OpenFacebook
import logging

log = logging.getLogger(__name__)

# Subclass AbstractUser
class User(AbstractUser):
    uuid = UUIDField(unique=True)
    profile_picture = models.URLField(max_length=500)

    def __unicode__(self):
        return self.get_full_name()

    @cached_property
    def facebook_account(self):
        try:
            return SocialAccount.objects.get(user=self, provider='facebook')
        except:
            return

    @cached_property
    def facebook_access_token(self):
        try:
            return self.facebook_account.socialtoken_set.last().token
        except:
            return

    @cached_property
    def facebook(self):
        return OpenFacebook(self.facebook_access_token)

    @cached_property
    def in_facebook_group(self):
        return any([settings.FACEBOOK_NZPG_ID == group['id'] for group in self.facebook.get('me/groups')['data']])

    @property
    def can_post(self):
        from nzpg.apps.governance.models import NaughtyCorner
        return not NaughtyCorner.objects.has_user(self)

    @property
    def posts(self):
        from nzpg.apps.stream.models import Post
        return self.trackable_set.instance_of(Post)

    @property
    def albums(self):
        from nzpg.apps.photos.models import Album
        return self.trackable_set.instance_of(Album)

    @property
    def photos(self):
        from nzpg.apps.photos.models import Photo
        return self.trackable_set.instance_of(Photo)

    def get_absolute_url(self):
        return reverse('users:detail', args=[self.username])
