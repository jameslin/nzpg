# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db.models import Count
from django.contrib.auth import get_user_model
from nzpg.apps.contrib.models import Content
from polymorphic import PolymorphicQuerySet
import logging

log = logging.getLogger(__name__)


class VoteQuerySet(PolymorphicQuerySet):
    def vote(self, user, item, **kwarg):
        log.info("{} votes {}".format(user, item))
        return self.create(created_by=user, item=item, **kwarg)

    def voted(self, user, item):
        return self.filter(created_by=user, item=item).count() > 0

    def unvote(self, user, item):
        log.info("{} unvotes {}".format(self, item))
        try:
            vote = self.model.objects.get(item=item, created_by=user)
            vote.delete()
            return True
        except self.model.DoesNotExist:
            pass
        return False

    def total_received(self, user):
        return self.filter(item__created_by=user).count()

    def total_given(self, user):
        return self.filter(created_by=user).count()

    def top_voted_content(self):
        return Content.objects.annotate(voted_count=Count('votes')).order_by('-voted_count')
