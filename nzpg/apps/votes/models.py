# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from nzpg.apps.contrib.models import Trackable
from nzpg.apps.contrib.managers import ContentManager
from django.db import models
from .managers import VoteQuerySet


class Vote(Trackable):
    item = models.ForeignKey(Trackable, related_name='votes')
    objects = ContentManager(VoteQuerySet)

    # http://stackoverflow.com/questions/28799949/django-polymorphic-models-with-unique-together?noredirect=1#comment45874235_28799949
    # class Meta:
        # unique_together = ['item', 'created_by']

    def get_absolute_url(self):
        return self.item.get_absolute_url()

    def __unicode__(self):
        return str(self.item)
