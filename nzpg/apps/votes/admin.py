# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.contrib import admin
from .models import Vote
from nzpg.apps.contrib.admin import TrackableAdmin


class VoteAdmin(TrackableAdmin):
    list_display = ['item', 'created_by', 'created_at']
    search_fields = ['created_by', 'item']
    readonly_fields =['item', 'created_by']
    base_model = Vote
    polymorphic_list = True


admin.site.register(Vote, VoteAdmin)
