# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import RedirectView
from nzpg.apps.contrib.models import Trackable
from .models import Vote
from django.http import JsonResponse
from nzpg.apps.contrib.views import ActiveUserRequiredMixin
from braces.views import LoginRequiredMixin


class VoteMixin(object):
    permanent = False

    def toggle_vote(self, user, item, *args, **kwargs):
        action = 'vote'
        if Vote.objects.voted(user, item):
            Vote.objects.unvote(user, item)
            action = 'unvote'
        else:
            Vote.objects.vote(user, item)
        return action == 'vote'

    def get_vote_item(self, *args, **kwargs):
        raise NotImplemented()

    def get_context_data(self, *args, **kwargs):
        ctx = super(VoteMixin, self).get_context_data(*args, **kwargs)
        if self.request.user.is_authenticated():
            ctx['voted'] = Vote.objects.voted(self.request.user, self.get_vote_item())
        return ctx
