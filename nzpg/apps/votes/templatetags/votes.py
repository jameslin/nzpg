from django import template

register = template.Library()


@register.filter
def voted(content, user):
    try:
        if content.votes.filter(created_by=user).count():
            return True
    except:
        pass
    return False
