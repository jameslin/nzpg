from haystack import indexes
from nzpg.apps.stream.models import Post
from nzpg.apps.photos.models import Photo, Album
from nzpg.apps.competitions.models import Competition, Event


class ContentIndexMixin(object):
    model = None

    def get_model(self):
        return self.model

    def index_queryset(self, using=None):
        return self.model.objects.active_content()


# class PostIndex(ContentIndexMixin, indexes.SearchIndex, indexes.Indexable):
    # model = Post
    # text = indexes.CharField(document=True, use_template=True)
    # created_by = indexes.CharField(model_attr='created_by')
    # created_at = indexes.DateTimeField(model_attr='created_at')
    # updated_at = indexes.DateTimeField(model_attr='updated_at')
    # description = indexes.CharField(model_attr='description')


# class PhotoIndex(ContentIndexMixin, indexes.SearchIndex, indexes.Indexable):
    # model = Photo
    # text = indexes.CharField(document=True, use_template=True)
    # created_by = indexes.CharField(model_attr='created_by')
    # created_at = indexes.DateTimeField(model_attr='created_at')
    # updated_at = indexes.DateTimeField(model_attr='updated_at')
    # title = indexes.CharField(model_attr='title')
    # description = indexes.CharField(model_attr='description')


# class AlbumIndex(ContentIndexMixin, indexes.SearchIndex, indexes.Indexable):
    # model = Album
    # text = indexes.CharField(document=True, use_template=True)
    # created_by = indexes.CharField(model_attr='created_by')
    # created_at = indexes.DateTimeField(model_attr='created_at')
    # updated_at = indexes.DateTimeField(model_attr='updated_at')
    # title = indexes.CharField(model_attr='title')
    # description = indexes.CharField(model_attr='description')


class CompetitionIndex(ContentIndexMixin, indexes.SearchIndex, indexes.Indexable):
    model = Competition
    text = indexes.CharField(document=True, use_template=True)
    created_by = indexes.CharField(model_attr='created_by')
    created_at = indexes.DateTimeField(model_attr='created_at')
    updated_at = indexes.DateTimeField(model_attr='updated_at')
    title = indexes.CharField(model_attr='title')
    description = indexes.CharField(model_attr='description')


# class EventIndex(ContentIndexMixin, indexes.SearchIndex, indexes.Indexable):
    # model = Event
    # text = indexes.CharField(document=True, use_template=True)
    # created_by = indexes.CharField(model_attr='created_by')
    # created_at = indexes.DateTimeField(model_attr='created_at')
    # updated_at = indexes.DateTimeField(model_attr='updated_at')
    # title = indexes.CharField(model_attr='title')
    # description = indexes.CharField(model_attr='description')
    # location = indexes.CharField(model_attr='description')
