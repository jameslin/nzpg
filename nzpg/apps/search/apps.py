from django.apps.config import AppConfig


class SearchConfig(AppConfig):
    name = 'nzpg.apps.search'
    verbose_name = 'Search'

    def ready(self):
        import search_indexes
