# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from nzpg.apps.contrib.models import Trackable
from django.db import models
from django.conf import settings


class Badge(Trackable):
    CHOICES = (
        ('Bronze Donor', 'Bronze Donor'),
        ('Silver Donor', 'Silver Donor'),
        ('Gold Donor', 'Gold Donor'),
    )
    name = models.CharField(max_length=15, choices=CHOICES)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __unicode__(self):
        return self.name
