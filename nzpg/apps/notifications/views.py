# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import RedirectView
from .models import Notification


class NotificationView(RedirectView):
    def get_notification(self):
        return Notification.objects.get(uuid=self.kwargs.get('uuid'))

    def get_redirect_url(self, *args, **kwargs):
        notification = self.get_notification()
        notification.is_read = True
        notification.save()
        return notification.item.get_absolute_url()
