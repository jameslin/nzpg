# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.utils.timezone import now
from nzpg.celery import app
from nzpg.apps.notifications.models import Notification, Notified
from django.template.loader import render_to_string
from django.utils.timezone import now
from .models import Competition, Event

@app.task(bind=True)
def competition_finished(self):
    """
    notify the likers when voting finished
    """
    notify_reason = 'finished'
    for competition in Competition.objects.finished().exclude(notified__reason=notify_reason):
        # notify the people who liked the photos
        liked = []
        message = render_to_string('notifications/tasks/competition_finished.html', {'competition': competition})
        for like in Like.objects.filter(item__in=competition.photos.all()):
            if like.created_by not in liked:
                liked.append(like.created_by)
                Notification.objects.create(created_by=competition.created_by, item=competition, user=like.created_by, message=message)

        # marks the competition is notified
        Notified.objects.create(item=competition, reason=notify_reason)


@app.task(bind=True)
def event_tomorrow(self):
    notify_reason = 'tomorrow'
    for event in Event.objects.tomorrow().exclude(notifieds__reason=notify_reason):
        message = render_to_string('notifications/tasks/event_tomorrow.html', {'event': event})
        for user in event.going.all():
            Notification.objects.create(created_by=event.created_by, item=event, user=user, message=message)
        for user in event.maybe.all():
            Notification.objects.create(created_by=event.created_by, item=event, user=user, message=message)

        # marks the event is notified
        Notified.objects.create(item=event, reason=tomorrow)
