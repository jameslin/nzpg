from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^(?P<uuid>.+)/?$', views.NotificationView.as_view(), name="notification"),
)
