# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.test import TestCase
from django_dynamic_fixture import G
from django.contrib.auth import get_user_model
from nzpg.apps.stream.models import Post
from nzpg.apps.likes.models import Like
from nzpg.apps.comments.models import Comment
from .models import Notification


User = get_user_model()

class NotificationLikeTestCase(TestCase):

    def test_signal(self):
        user = G(User)
        user1 = G(User)
        p = Post.objects.create(created_by=user)
        like = Like.objects.likes(user1, p)
        self.assertTrue(Notification.objects.filter(item=like.item).count())

    def test_aggregation(self):
        user = G(User)
        user1 = G(User)
        p = Post.objects.create(created_by=user)
        like = Like.objects.likes(user1, p)
        user2 = G(User)
        like1 = Like.objects.likes(user2, p)
        self.assertEqual(Notification.objects.filter(item=like.item).count(), 1)

class NotificationCommentTestCase(TestCase):

    def test_signal(self):
        user = G(User)
        user1 = G(User)
        p = Post.objects.create(created_by=user)
        comment = Comment.objects.make_comment(user1, p, '')
        self.assertTrue(Notification.objects.filter(item=comment.content).count())

    def test_aggregation(self):
        user = G(User)
        user1 = G(User)
        p = Post.objects.create(created_by=user)
        comment = Comment.objects.make_comment(user1, p, '')
        user2 = G(User)
        comment = Comment.objects.make_comment(user2, p, '')
        self.assertEqual(Notification.objects.filter(item=comment.content).count(), 1)
