from django.apps.config import AppConfig


class NotificationsConfig(AppConfig):
    name = 'nzpg.apps.notifications'
    verbose_name = 'Notifications'

    def ready(self):
        import signals
