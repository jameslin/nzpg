# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from nzpg.apps.comments.models import Comment
from nzpg.apps.likes.models import Like
from .models import Notification


@receiver(post_save, sender=Comment)
def comment_made(sender, instance, created, *args, **kwargs):
    source = 'comment'
    comment = instance
    if created and not comment.created_by == comment.content.created_by:
        # try to aggregate
        try:
            notification = Notification.objects.get(item=comment.content, is_read=False, source=source)
            notification.message = render_to_string('notifications/signals/comment_aggregate.html', {'comment': comment})
            notification.save()
        except Notification.DoesNotExist:
            message = render_to_string('notifications/signals/comment.html', {'comment': comment})
            Notification.objects.create(created_by=comment.created_by, message=message, user=comment.content.created_by, item=comment.content, source=source)


@receiver(post_save, sender=Like)
def like_made(sender, instance, created, *args, **kwargs):
    like = instance
    source = 'like'
    if created and not like.created_by == like.item.created_by:
        # try to aggregate
        try:
            notification = Notification.objects.get(item=like.item, is_read=False, source=source)
            notification.message = render_to_string('notifications/signals/like_aggregate.html', {'like': like})
            notification.save()
        except Notification.DoesNotExist:
            message = render_to_string('notifications/signals/like.html', {'like': like})
            Notification.objects.create(created_by=like.created_by, message=message, user=like.item.created_by, item=like.item, source=source)
