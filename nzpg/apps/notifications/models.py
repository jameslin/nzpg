# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db import models
from django.conf import settings
from nzpg.apps.contrib.models import Trackable
from django.core.urlresolvers import reverse_lazy


class Notification(Trackable):
    item = models.ForeignKey(Trackable, related_name="notifications")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="notifications")
    message = models.TextField()
    is_read = models.BooleanField(default=False)
    # so we can track this notification was created by eg. like, useful for notification aggregation
    source = models.CharField(max_length=25, blank=False)

    class Meta:
        ordering = ['-id']

    def read(self):
        self.is_read = True
        self.save()

    def get_absolute_url(self):
        return reverse_lazy('notifications:notification', args=[self.uuid])


class Notified(Trackable):
    item = models.ForeignKey(Trackable, related_name="notifieds")
    # just a string to explain the notification for
    reason = models.CharField(max_length=25)
