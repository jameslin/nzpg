from django.apps.config import AppConfig


class GovernanceConfig(AppConfig):
    name = 'nzpg.apps.governance'
    verbose_name = 'Governance'

    def ready(self):
        import signals
        import templatetags
