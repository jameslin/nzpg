from django import template
from ..models import Inappropriate

register = template.Library()


@register.assignment_tag
def is_reported_by(content, user):
    try:
        return Inappropriate.objects.is_reported_by(content, user)
    except:
        pass
    return False
