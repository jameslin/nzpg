# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.test import TestCase
from django_dynamic_fixture import G
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.exceptions import ValidationError
from nzpg.apps.stream.models import Post
from .models import Inappropriate, NaughtyCorner

User = get_user_model()


class InappropriateTest(TestCase):
    def test_flag(self):
        num = settings.INAPPROPRIATE_THRESHOLD
        user = G(User)
        me = G(User)
        p = Post.objects.create(created_by=user)
        for i in range(num):
            self.assertFalse(NaughtyCorner.objects.has_user(user))
            reporter = G(User)
            Inappropriate.objects.create(content=p, created_by=reporter)
        self.assertFalse(p.is_active)

    def already_reported(self):
        me = G(User)
        p = Post.objects.create(created_by=user)
        Inappropriate.objects.create(content=p, created_by=me)
        with self.assertRaises(ValidationError):
            Inappropriate.objects.create(content=p, created_by=me)


class NaugtyCornerTest(TestCase):
    def test_add(self):
        num = settings.INAPPROPRIATE_THRESHOLD
        user = G(User)
        me = G(User)
        p = Post.objects.create(created_by=user)
        for i in range(num):
            self.assertFalse(NaughtyCorner.objects.has_user(user))
            reporter = G(User)
            Inappropriate.objects.create(content=p, created_by=reporter)
        self.assertTrue(NaughtyCorner.objects.has_user(user))
