# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from polymorphic import PolymorphicManager
from nzpg.apps.contrib.models import Trackable, Content
from .managers import NaughtyCornerQuerySet, InappropriateQuerySet


class Inappropriate(Trackable):
    content = models.ForeignKey(Content)
    reason = models.CharField(max_length=255, blank=True)
    objects = PolymorphicManager(InappropriateQuerySet)

    def clean(self):
        super(Inappropriate, self).clean()
        if Inappropriate.objects.is_reported_by(self.content_id, self.created_by_id):
            raise ValidationError('You have already reported this.')

    def save(self, *args, **kwargs):
        self.clean()
        return super(Inappropriate, self).save(*args, **kwargs)


class NaughtyCorner(Trackable):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    till = models.DateTimeField()
    objects = PolymorphicManager(NaughtyCornerQuerySet)
