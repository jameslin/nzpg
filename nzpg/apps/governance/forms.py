from django.forms import ModelForm
from .models import Inappropriate


class InappropriateForm(ModelForm):
    class Meta:
        model = Inappropriate
        fields = ['reason']
