# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from polymorphic import PolymorphicQuerySet
from django.utils.timezone import now
from datetime import timedelta


class InappropriateQuerySet(PolymorphicQuerySet):
    def is_reported_by(self, content, user):
        return self.filter(content=content, created_by=user).count() > 0


class NaughtyCornerQuerySet(PolymorphicQuerySet):
    def has_user(self, user):
        return self.filter(user=user, till__gte=now()).count() > 0

    def add_user(self, user, minutes=1440):
        self.create(user=user, till=now() + timedelta(minutes=minutes), created_by=user)
