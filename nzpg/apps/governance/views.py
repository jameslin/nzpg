# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import FormView, ListView
from django.contrib import messages
from .models import Inappropriate
from .forms import InappropriateForm
from nzpg.apps.contrib.models import Content
from nzpg.apps.contrib.views import ActiveUserRequiredMixin
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy


class ReportInappropriateView(ActiveUserRequiredMixin, FormView):
    form_class = InappropriateForm
    template_name = 'governance/report_inappropriate.html'
    success_url = reverse_lazy('stream:home')
    # dont just throw 403 when user is not logged in
    raise_exception = False

    def get_content(self):
        return Content.objects.get(uuid=self.kwargs.get('uuid'))

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.content = self.get_content()
        instance.created_by = self.request.user
        instance.save()
        messages.add_message(self.request, messages.INFO, 'Thank you for reporting the content.')
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, *args, **kwargs):
        content = self.get_content
        ctx = super(ReportInappropriateView, self).get_context_data(*args, **kwargs)
        ctx['already_reported'] = Inappropriate.objects.filter(content=content, created_by=self.request.user).count() > 0
        ctx['content'] = content
        return ctx


class InactiveContentListView(ListView):

    model = Content
    paginate_by = 20
    queryset = Content.objects.inactive_content()
