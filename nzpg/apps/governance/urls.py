from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^report-inappropriate/(?P<uuid>.+)/?$', views.ReportInappropriateView.as_view(), name="report-inappropriate"),
)
