from django.dispatch import receiver
from django.db.models.signals import post_save
from django.conf import settings
from .models import Inappropriate, NaughtyCorner


@receiver(post_save, sender=Inappropriate)
def after_received_inappropriate(sender, instance, created, *args, **kwargs):
    """
    Deactivates content if number of inappropriate reported reached.
    """
    if instance.content.is_active and instance.content.inappropriate_set.count() >= settings.INAPPROPRIATE_THRESHOLD:
        instance.content.deactivate()
        # freeze the user for a time period
        NaughtyCorner.objects.add_user(instance.content.created_by)
