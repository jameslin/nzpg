from django.contrib import admin
from .models import Inappropriate
from nzpg.apps.contrib.admin import TrackableAdminMixin


class InappropriateAdmin(TrackableAdminMixin, admin.ModelAdmin):
    list_display = ['describe', 'reason', 'created_by', 'created_at']
    search_fields = ['created_by', 'content', 'reason']

    def describe(self, obj):
        return unicode(obj.content)
    describe.short_description = 'Describe'

admin.site.register(Inappropriate, InappropriateAdmin)
