from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^(?P<name>.+)/?$', views.TaggedItemsView.as_view(), name="tag"),
)
