from taggit.models import Tag, TaggedItem
from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404


class TaggedItemsView(ListView):
    model = TaggedItem
    paginate_by = 10
    context_object_name = 'items'
    template_name = 'tags/tag.html'

    def get_tag(self):
        return get_object_or_404(Tag, name=self.kwargs.get('name'))

    def get_queryset(self):
        tag = self.get_tag()
        items = TaggedItem.objects.filter(tag__name__in=[tag])
        return items

    def get_context_data(self, *args, **kwargs):
        ctx = super(TaggedItemsView, self).get_context_data(*args, **kwargs)
        ctx['tag'] = self.get_tag()
        return ctx
