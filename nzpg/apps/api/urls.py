# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import routers
from django.conf.urls import url, include

router = routers.SimpleRouter()

urlpatterns = router.urls
urlpatterns += [
    url('^comments/', include('nzpg.apps.api.comments.urls', namespace='comments')),
    url('^users/', include('nzpg.apps.api.users.urls', namespace='users')),
    url('^votes/', include('nzpg.apps.api.votes.urls', namespace='votes')),
]
