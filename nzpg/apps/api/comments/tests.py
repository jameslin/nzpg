from django.test import TestCase
from rest_framework.test import APIRequestFactory, APIClient, force_authenticate
from nzpg.apps.users.models import User
from nzpg.apps.stream.models import Post
from nzpg.apps.comments.models import Comment
from .views import CommentViewSet


class CommentTestCase(TestCase):

    def test_comment(self):
        self.assertEqual(Comment.objects.count(), 0)
        user = User.objects.create_user('testuser', 'test@test.com', 'password')
        post = Post.objects.create(created_by=user)
        factory = APIRequestFactory()
        req = factory.post('/api/comments/comments/?content_uuid={}'.format(post.uuid), data={
            'message': 'abc'
        }, format='json')
        force_authenticate(req, user=user)
        CommentViewSet.as_view({'post': 'create'})(req)
        self.assertEqual(Comment.objects.count(), 1)

    def test_comment_1(self):
        self.assertEqual(Comment.objects.count(), 0)
        user = User.objects.create_user('testuser', 'test@test.com', 'password')
        post = Post.objects.create(created_by=user)
        client = APIClient()
        client.login(username='testuser', password='password')
        result = client.post('/api/comments/comments/?content_uuid={}'.format(post.uuid), data={
            'message': 'abc'
        }, format='json')
        self.assertEqual(Comment.objects.count(), 1)
