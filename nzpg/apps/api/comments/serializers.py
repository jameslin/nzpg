# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import serializers
from nzpg.apps.contrib.models import Content
from ..contrib.serializers import ContentSerializer
from ...comments.models import Comment
from ..users.serializers import UserSerializer

class CommentSerializer(ContentSerializer):

    created_by = UserSerializer(required=False, read_only=True)
    content = serializers.PrimaryKeyRelatedField(queryset=Content.objects.all(), required=False)

    class Meta:
        model = Comment
