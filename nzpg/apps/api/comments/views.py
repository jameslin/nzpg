# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import viewsets
from rest_framework.exceptions import APIException
from .serializers import CommentSerializer
from ...contrib.models import Content


class CommentViewSet(viewsets.ModelViewSet):

    serializer_class = CommentSerializer

    def get_content(self):
        return Content.objects.get(uuid=self.request.GET.get('content_uuid'))

    def get_queryset(self):
        return self.get_content().comments.active_content()

    def perform_create(self, serializer):
        serializer.save(content=self.get_content(), created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_destroy(self, instance):
        if not self.request.user.is_superuser and not instance.created_by == self.request.user:
            raise APIException("Cannot delete content that does not belong to you.")
        instance.delete()
