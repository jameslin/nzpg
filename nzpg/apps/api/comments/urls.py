# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import routers
from .views import CommentViewSet

router = routers.SimpleRouter()
router.register(r'comments', CommentViewSet, base_name="comments")
urlpatterns = router.urls
