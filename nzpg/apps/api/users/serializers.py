# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import serializers
from ...comments.models import Comment
from ...users.models import User


class UserSerializer(serializers.ModelSerializer):
    profile_link = serializers.SerializerMethodField()

    def get_profile_link(self, obj):
        return obj.get_absolute_url()

    class Meta:
        model = User
