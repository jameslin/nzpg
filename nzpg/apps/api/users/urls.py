# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf.urls import patterns, include, url
from rest_framework import routers
from .views import CurrentUserView

urlpatterns = patterns('',
    url('^current/?$', CurrentUserView.as_view(), name='current_user'),
)
