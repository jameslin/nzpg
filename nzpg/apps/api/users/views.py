# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UserSerializer
from rest_framework.permissions import IsAuthenticated


class CurrentUserView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        return Response(UserSerializer(request.user).data)
