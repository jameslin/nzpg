# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import permissions


class UserIsActive(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.can_post
