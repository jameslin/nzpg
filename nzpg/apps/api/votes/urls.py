# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf.urls import patterns, include, url
from .views import VoteStateView

urlpatterns = patterns('',
    url('^state/(?P<uuid>.+)$', VoteStateView.as_view(), name='state'),
)
