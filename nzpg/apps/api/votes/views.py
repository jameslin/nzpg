# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework.views import APIView
from rest_framework.response import Response
from nzpg.apps.contrib.models import Trackable


class VoteStateView(APIView):

    def get(self, request, uuid, format=None):
        obj = Trackable.objects.get(uuid=uuid)
        data = {
            'votes': obj.votes.count(),
            'is_author': obj.created_by == request.user,
            'voted': obj.votes.filter(created_by=request.user).count() > 0 if request.user.is_authenticated() else False,
            'is_logged_in': request.user.is_authenticated(),
        }
        return Response(data)
