# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import serializers
from nzpg.apps.contrib.models import Trackable, Content
from ...comments.models import Comment
from ..users.serializers import UserSerializer


class TrackableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Trackable


class ContentSerializer(TrackableSerializer):

    class Meta:
        model = Content
