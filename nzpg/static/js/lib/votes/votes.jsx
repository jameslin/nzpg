var React = require('react');
var $ = require('app/common/jquery');
var funcs = require('app/common/functions');
var moment = require('moment');

var VoteBox = React.createClass({
    getInitialState: function(){
        return this.getState()
    },
    getState: function(){
        var result = null;
        $.ajax({
            url: this.props.state_href,
            success: function(_result){ result = _result; },
            error: function(){ result = false; },
            dataType: 'json',
            async: false,
        });
        return result;
    },
    toggleVote: function(e){
        e.preventDefault();
        if (!this.state.is_logged_in){
            $('.header-login').first().click();
        }else{
            $.post(this.props.href, {'vote': true}, function(result){
                this.setState(this.getState());
            }.bind(this), 'json');
        }
    },
    render: function(){
      var now = Date.now();
      var vote_text = (this.state.voted ? 'Unvote' : 'Vote');
      var display_vote_counts = '';
      if (this.state.votes == 1){
          display_vote_counts = "1 person voted this";
      }else{
          display_vote_counts = this.state.votes + " people voted this";
      }
      var display_text;
      if (now < this.props.start_date){
          display_text = 'Voting will start on '+moment(this.props.start_date).format('LLL');
      }
      else if (now > this.props.end_date){
          display_text = 'Voting has finished, ' + display_vote_counts;
      }
      else if (this.props.votes_left == 0){
          display_text = 'You cannot cast anymore votes, '+ display_vote_counts;
      }
      else{
          display_text = display_vote_counts;
      }
      var display_block =
            <div className="vote-text">
                <span>{display_text}</span>
            </div>

      var button_node;
      if (!this.state.is_author && this.state.is_logged_in && now > this.props.start_date && now < this.props.end_date
            && (this.state.voted || (!this.state.voted && this.props.votes_left))
         ){
          button_node =
              <button onClick={this.toggleVote} className="rounded-btn">
                <i className="icon icon-mail-inbox"></i>
                <span className="btn-label">{vote_text}</span>
              </button>
      }

      return (
        <div className="voteBox">
            {display_block}
            {button_node}
        </div>
      );
    }
})


module.exports.VoteBox = VoteBox;
