"use strict";

var $ = require('jquery');
var jQBridget = require('jquery-bridget');
var React = require('react');
var VoteBox = require('app/votes/votes.jsx').VoteBox;
var moment = require('moment');


function VoteBoxPlugin( element, options ) {
  this.element = $( element );
  this.options = $.extend( true, {}, this.options, options );
  this._init();
}

VoteBoxPlugin.prototype.options = {};

VoteBoxPlugin.prototype._init = function() {
   React.render(
        <VoteBox votes_left={parseInt(this.element.data('votes-left'))}
                 state_href={this.element.data('state-href')} 
                 href={this.element.data('href')} 
                 start_date={moment(this.element.data('start-date')).valueOf()}
                 end_date={moment(this.element.data('end-date')).valueOf()}/>,
        this.element.get(0)
    );
};

$.bridget('votebox', VoteBoxPlugin);
