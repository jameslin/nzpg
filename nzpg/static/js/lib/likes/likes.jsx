var React = require('react');
var $ = require('app/common/jquery');
var LikeStore = require('app/likes/stores');
var LikeActions = require('app/likes/actions');

var LikeDisplay = React.createClass({
    getInitialState: function(){
        return LikeStore.getState();
    },
    componentDidMount(){
        LikeActions.getState(this.props.href);
        LikeStore.listen(this.onToggleLike);
    },
    componentWillUnmount(){
        LikeStore.unlisten(this.onToggleLike);
    },
    onToggleLike: function(state){
        this.setState(state);
    },
    render: function(){
      var people = 'people';
      if (this.state.count == 1){ people = 'person'; }
      return (
            <div className="like-text">
                <span>{this.state.count} {people} liked this</span>
            </div>
      )
    }
})

var LikeBox = React.createClass({
    getInitialState: function(){
        return LikeStore.getState();
    },
    componentDidMount(){
        LikeActions.getState(this.props.href);
        LikeStore.listen(this.onToggleLike);
    },
    componentWillUnmount(){
        LikeStore.unlisten(this.onToggleLike);
    },
    toggleLike: function(e){
        e.preventDefault();
        if (!this.props.is_logged_in){
            $('.header-login').first().click();
        }else{
            LikeActions.toggleLike(this.props.href);
        }
    },
    onToggleLike: function(state){
        this.setState(state);
    },
    render: function(){
      var like_text = "";
      if (this.state.liked){
        like_text = <a href="#" onClick={this.toggleLike} className="rounded-btn btn-primary">
          <i className="icon icon-heart-1"></i>
          <span className="btn-label">Liked</span>
        </a>
      }else{
        like_text = <a href="#" onClick={this.toggleLike} className="rounded-btn">
          <i className="icon icon-heart-1"></i>
          <span className="btn-label">Like</span>
        </a>
      }
      return (
        <div className="likeBox">
            {like_text}
        </div>
      );
    }
})


module.exports.LikeBox = LikeBox;
module.exports.LikeDisplay = LikeDisplay;
