"use strict";

var alt = require('app/common/alt');
var LikeActions = require('app/likes/actions');

class LikeStore {
    constructor(){
        this.liked = false;
        this.count = 0;
        this.bindListeners({
            handleLikeAction: LikeActions.TOGGLE_LIKE,
            handleGetStateAction: LikeActions.GET_STATE
        });
    }

    handleLikeAction(result){
        this.liked = result.liked;
        this.count = result.count;
    }

    handleGetStateAction(result){
        this.liked = result.liked;
        this.count = result.count;
    }
}

module.exports = alt.createStore(LikeStore, 'LikeStore');
