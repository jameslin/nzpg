"use strict";

var alt = require('app/common/alt');
var funcs = require('app/common/functions');
var $ = require('app/common/jquery');

class LikeActions {
    toggleLike(href){
        $.post(href, function(result){
            this.dispatch(result);
        }.bind(this), 'json');
    }

    getState(href){
        $.get(href, function(result){
            this.dispatch(result);
        }.bind(this), 'json');
    }
}

module.exports = alt.createActions(LikeActions);
