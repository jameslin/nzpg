"use strict";

var $ = require('jquery');
var jQBridget = require('jquery-bridget');
var React = require('react');
var LikeBox = require('app/likes/likes.jsx').LikeBox;
var LikeDisplay = require('app/likes/likes.jsx').LikeDisplay;
var funcs = require('app/common/functions');


function LikeDisplayPlugin( element, options ) {
  this.element = $( element );
  this.options = $.extend( true, {}, this.options, options );
  this._init();
}

LikeDisplayPlugin.prototype.options = {};

LikeDisplayPlugin.prototype._init = function() {
   React.render(
        <LikeDisplay />,
        this.element.get(0)
    );
};
$.bridget('likedisplay', LikeDisplayPlugin);

function LikeBoxPlugin( element, options ) {
  this.element = $( element );
  this.options = $.extend( true, {}, this.options, options );
  this._init();
}

LikeBoxPlugin.prototype.options = {};

LikeBoxPlugin.prototype._init = function() {
   React.render(
        <LikeBox is_logged_in={funcs.get_current_user()} href={this.element.data('href')}/>,
        this.element.get(0)
    );
};

$.bridget('likebox', LikeBoxPlugin);
