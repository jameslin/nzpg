var React = require('react');
var $ = require('app/common/jquery');
var moment = require('moment');

var CommentBox = React.createClass({
    getInitialState: function(){
      return {data: []};
    },
    handleCommentSubmit: function(comment){
        this.refreshComments();
    },
    apiURL: function(){
        return this.props.apiUrl+"?content_uuid="+this.props.contentUuid;
    },
    refreshComments: function(){
        $.get(this.apiURL(), function(data){
            this.setState({data: data});
        }.bind(this), 'json');
    },
    componentDidMount: function(){
        this.refreshComments();
    },
    render: function(){
      return (
        <div className="comment-box">
          <CommentList data={this.state.data} />
          <CommentForm onCommentSubmit={this.handleCommentSubmit} apiURL={this.apiURL()} />
        </div>
      );
    }
});

var CommentList = React.createClass({
    render: function(){
      return (
        <div className="comment-list">
          {
            this.props.data.map(function (comment) {
              return(
                <Comment comment={comment}></Comment>
              );
            })
          }
        </div>
      );
    }
});

var Comment = React.createClass({
    render: function(){
      var created_at = moment(this.props.comment.created_at).format('lll');
      return (
        <div className="comment-entry">
          <div className="form-group">
            <span className="circle-btn header-avatar">
              <a href={this.props.comment.created_by.profile_link}><img src={this.props.comment.created_by.profile_picture} /></a>
            </span>
            <div className="comment-details">
              <a href="#">{this.props.comment.created_by.first_name + ' ' + this.props.comment.created_by.last_name}</a><span className="comment-date">{created_at}</span><br />
              {this.props.comment.message}
            </div>
          </div>
        </div>
      );
    }
});

var CommentForm = React.createClass({
    getInitialState: function(){
        return {user: null};
    },
    handleSubmit: function(){
        var msg_node = React.findDOMNode(this.refs.message);
        var msg = msg_node.value.trim();
        $.post(this.props.apiURL, {message: msg}, function(data){
            this.props.onCommentSubmit();
        }.bind(this));
        msg_node.value = '';
    },
    getProfile: function(){
        $.get('/api/users/current/', function(user){
            this.setState({
                user:user,
            });
        }.bind(this), 'json')
    },
    componentDidMount: function(){
        this.getProfile();
    },
    render: function(){
      if (this.state.user){
          return (
            <div className="comment-form">
                <div className="form-group">
                  <span className="circle-btn header-avatar">
                    <a href="#"><img src={this.state.user.profile_picture} /></a>
                  </span>
                  <div className="comment-details">
                    <div className="comment-arrow"></div>
                    <textarea ref="message" placeholder="Add a comment"></textarea>
                  </div>
                </div>
                <div className="form-group text-right">
                  <button className="rounded-btn" onClick={this.handleSubmit}>
                    <span className="btn-label">Submit</span>
                  </button>
                </div>
            </div>
          );
      }else{
          return (<div></div>)
      }
    }
});

module.exports.Comment = Comment;
module.exports.CommentList = CommentList;
module.exports.CommentBox = CommentBox;
module.exports.CommentForm = CommentForm;
