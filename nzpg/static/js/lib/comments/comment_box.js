"use strict";

var $ = require('jquery');
var React = require('react');
var jQBridget = require('jquery-bridget');
var CommentBox = require('app/comments/comment_box.jsx').CommentBox;

function CommentBoxPlugin( element, options ) {
  this.element = $( element );
  this.options = $.extend( true, {}, this.options, options );
  this._init();
}

CommentBoxPlugin.prototype.options = {};

CommentBoxPlugin.prototype._init = function() {
   React.render(
        <CommentBox contentUuid={this.element.data('uuid')} apiUrl={this.element.data('api-url')}/>,
        this.element.get(0)
    );
};

$.bridget('commentbox', CommentBoxPlugin);
