"use strict";

var $ = require('jquery');
var Isotope = require('isotope-layout');
var jQBridget = require('jquery-bridget');

$(function(){
    $.bridget('isotope', Isotope);

    $(window).load(function(){
        // init Isotope
        var $container = $('.isotope').isotope({
            itemSelector: '.item',
            percentPosition: true,
            layoutMode: 'masonry',
            position: 'relative',
            getSortData: {
                name: '.item-title',
                date: '.date-tag',
                votes: function( itemElem ) { // function
                  var weight = $( itemElem ).find('.votes').text();
                  return parseFloat( weight.replace( /[\(\)]/g, '') );
                }
            }
        });

        // filter functions
        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function() {
                var number = $(this).find('.number').text();
                return parseInt( number, 10 ) > 50;
            },
            // show if name ends with -ium
            ium: function() {
                var name = $(this).find('.name').text();
                return name.match( /ium$/ );
            }
        };

        // bind filter button click
        $('#isotope-filter').on( 'click', 'button', function() {
            var filterValue = $( this ).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[ filterValue ] || filterValue;
            $container.isotope({ filter: filterValue });
        });

        // bind sort button click
        $('#isotope-sort').on( 'click', 'button', function() {
            var sortValue = $(this).data('sort-value');
            var descending = $(this).data('sort-descending');
            $container.isotope({ sortBy: sortValue, sortAscending: !descending});
        });

        // change active class on buttons
        $('.btn-group').each( function( i, buttonGroup ) {
            var $buttonGroup = $( buttonGroup );
            $buttonGroup.on( 'click', 'button', function() {
                $buttonGroup.find('.active').removeClass('active');
                $( this ).addClass('active');
            });
        });
    });
});

