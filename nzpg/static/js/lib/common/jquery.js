var $ = require('jquery');

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        var csrftoken = $('meta[name="csrf-token"]').attr('content');
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
});

module.exports = $;
