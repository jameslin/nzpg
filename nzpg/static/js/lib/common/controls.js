"use strict";

var $ = require('jquery');
require('app/comments/comment_box.js');
require('app/likes/likes.js');
require('app/votes/votes.js');

$(function(){
    // set up ajax like links
    $('.likebox').likebox();
    $('.likedisplay').likedisplay();

    // comment box javascript
    $('.comment-box').commentbox();
    $('.vote-box').votebox();
    console.log('loaded controls');
});
