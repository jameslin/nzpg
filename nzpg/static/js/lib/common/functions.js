"use strict";


var $ = require('jquery');

var get_current_user = function(){
    var result;
    $.ajax({
        url: '/api/users/current/',
        success: function(_result){ result = _result; },
        error: function(){ result = false; },
        async: false,
    });
    return result;
}

module.exports = {get_current_user:get_current_user};
