"use strict";

var $ = require('jquery');
var datepicker = require('jquery-ui/datepicker');

(function(){
    $('#id_entry_start_date, #id_entry_end_date, #id_vote_start_date, #id_vote_end_date').datepicker($.datepicker.regional['en-NZ']);
})();
