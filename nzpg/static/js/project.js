/* Project specific Javascript goes here. */
"use strict";

var $ = require('jquery');
global.jQuery = $;
var bootstrap = require('bootstrap');

(function(){
    // sets up the csrf on ajax calls
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            var csrftoken = $('meta[name="csrf-token"]').attr('content');
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    });

    function togglescroll () {
      $('body').on('touchstart', function(e){
        if ($('body').hasClass('noscroll')) {
          e.preventDefault();
        }
      });
    }

    togglescroll();

    var count = 0;

    $(".navbar-toggle").click(function () {
        $(this).toggleClass("active");
        if ( ++count % 2 === 1 ) {
            $("body").toggleClass("noscroll");
            $(".nav-overlay").toggleClass('show-element fadeIn');
            $(".navbar-nav").toggleClass('fadeInDown');
        }
        else {
            $("body").toggleClass("noscroll");
            $(".nav-overlay").removeClass('fadeIn');
            $(".navbar-nav").removeClass('fadeInDown');
            $(".nav-overlay").addClass('fadeOut');
            $(".navbar-nav").addClass('fadeOutDown');
            var delay = setTimeout(function(){
                $(".nav-overlay").removeClass('show-element fadeOut');
                $(".navbar-nav").removeClass('fadeOutDown');
            }, 700)
        }
        return false;
    });

    $(".header-login").click(function () {
        $("body").toggleClass("noscroll");
        $(".login-overlay").toggleClass('show-element fadeIn');
        $(".overlay-content").toggleClass('fadeInDown');
    });

    $(".header-search").click(function () {
        $(".search-overlay").toggleClass('show-element fadeIn');
        $(".overlay-content").toggleClass('fadeInDown');
    });

    $(".overlay-close").click(function () {
        $("body").removeClass("noscroll");
        $(".search-overlay").removeClass('fadeIn');
        $(".login-overlay").removeClass('fadeIn');
        $(".overlay-content").removeClass('fadeInDown');
        $(".search-overlay").addClass('fadeOut');
        $(".login-overlay").addClass('fadeOut');
        $(".overlay-content").addClass('fadeOutDown');
        var delay = setTimeout(function(){
            $(".search-overlay").removeClass('show-element fadeOut');
            $(".login-overlay").removeClass('show-element fadeOut');
            $(".overlay-content").removeClass('fadeOutDown');
            $('#header-search').val('');
        }, 700)
    });

    $(document).keydown(function(e) {
        if (e.keyCode == 27) {
            $("body").removeClass("noscroll");
            $(".navbar-toggle").removeClass("active");
            $(".nav-overlay").removeClass('fadeIn');
            $(".navbar-nav").removeClass('fadeInDown');
            $(".nav-overlay").addClass('fadeOut');
            $(".navbar-nav").addClass('fadeOutDown');
            $(".search-overlay").removeClass('fadeIn');
            $(".login-overlay").removeClass('fadeIn');
            $(".overlay-content").removeClass('fadeInDown');
            $(".search-overlay").addClass('fadeOut');
            $(".login-overlay").addClass('fadeOut');
            $(".overlay-content").addClass('fadeOutDown');
            var delay = setTimeout(function(){
                $(".nav-overlay").removeClass('show-element fadeOut');
                $(".navbar-nav").removeClass('fadeOutDown');
                $(".search-overlay").removeClass('show-element fadeOut');
                $(".login-overlay").removeClass('show-element fadeOut');
                $(".overlay-content").removeClass('fadeOutDown');
                $('#header-search').val('');
            }, 700)
        }
    });

    $('#search-tab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

    var search_wait;
    $('#search-form input[name=q]').keyup(function(){
        var q = $(this).val();
        clearTimeout(search_wait);
        search_wait = setTimeout(function(){
            $.get('/search/?q='+q, function(html){
                $('#search-content').html(html);
            });
        }, 2000);
    });

    $(".masonry-grid .item .item-wrapper .item-img").mouseenter(function() {
        $(".masonry-grid .item .item-wrapper").addClass('not-selected');
        $(this).parent().removeClass('not-selected');
    });

    $(".masonry-grid .item .item-wrapper .item-img").mouseleave(function() {
        $(".masonry-grid .item .item-wrapper").removeClass('not-selected');
    });

    $("textarea").focus(function() {
        $(this).addClass('not-selected');
    });

})();
