(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

require('app/common/controls');

},{"app/common/controls":2}],2:[function(require,module,exports){
"use strict";

var $ = require('jquery');
require('app/comments/comment_box.js');
require('app/likes/likes.js');
require('app/votes/votes.js');

$(function(){
    // set up ajax like links
    $('.likebox').likebox();
    $('.likedisplay').likedisplay();

    // comment box javascript
    $('.comment-box').commentbox();
    $('.vote-box').votebox();
    console.log('loaded controls');
});

},{"app/comments/comment_box.js":"app/comments/comment_box.js","app/likes/likes.js":"app/likes/likes.js","app/votes/votes.js":"app/votes/votes.js","jquery":"jquery"}]},{},[1]);
