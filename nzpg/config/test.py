from .common import Common
import os


class Test(Common):
    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DJANGO_DB_ENGINE', 'django.db.backends.mysql'),
            'NAME': os.environ.get('DJANGO_DB_NAME', 'nzpg'),
            'USER': os.environ.get('DJANGO_DB_USER', 'root'),
            'PASSWORD': os.environ.get('DJANGO_DB_PASSWORD', ''),
        }
    }
