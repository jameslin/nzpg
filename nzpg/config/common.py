# -*- coding: utf-8 -*-
"""
Django settings for nzpg project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from os.path import join, dirname
from datetime import timedelta
from configurations import Configuration, values

BASE_DIR = dirname(dirname(__file__))


class Common(Configuration):

    # APP CONFIGURATION
    DJANGO_APPS = (
        # Default Django apps:
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',

        # Useful template tags:
        # 'django.contrib.humanize',

        # Admin
        'django.contrib.admin',
    )
    THIRD_PARTY_APPS = (
        'crispy_forms',  # Form layouts
        #'avatar',  # for user avatars
        'allauth',  # registration
        'allauth.account',  # registration
        'allauth.socialaccount',  # registration
        'allauth.socialaccount.providers.facebook',
        'sorl.thumbnail',
        'taggit',
        'haystack',
        'floppyforms',
        'rest_framework',
    )

    # Apps specific for this project go here.
    LOCAL_APPS = (
        'nzpg.apps.users',  # custom users app
        'nzpg.apps.contrib',
        'nzpg.apps.photos',
        'nzpg.apps.stream',
        'nzpg.apps.classifieds',
        'nzpg.apps.competitions',
        'nzpg.apps.governance',
        'nzpg.apps.badges',
        'nzpg.apps.likes',
        'nzpg.apps.votes',
        'nzpg.apps.comments',
        'nzpg.apps.notifications',
        'nzpg.apps.search',
        'nzpg.apps.home',
        'nzpg.apps.api',
        # Your stuff: custom apps go here
    )

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
    INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
    # END APP CONFIGURATION

    # MIDDLEWARE CONFIGURATION
    MIDDLEWARE_CLASSES = (
        # Make sure djangosecure.middleware.SecurityMiddleware is listed first
        'djangosecure.middleware.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )
    # END MIDDLEWARE CONFIGURATION

    # MIGRATIONS CONFIGURATION
    MIGRATION_MODULES = {
        'sites': 'nzpg.apps.contrib.sites.migrations'
    }
    # END MIGRATIONS CONFIGURATION

    # DEBUG
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
    DEBUG = values.BooleanValue(False)

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
    TEMPLATE_DEBUG = DEBUG
    # END DEBUG

    # SECRET CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
    # Note: This key only used for development and testing.
    #       In production, this is changed to a values.SecretValue() setting
    SECRET_KEY = 'CHANGEME!!!'
    # END SECRET CONFIGURATION

    # FIXTURE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
    FIXTURE_DIRS = (
        join(BASE_DIR, 'fixtures'),
    )
    # END FIXTURE CONFIGURATION

    # EMAIL CONFIGURATION
    EMAIL_BACKEND = values.Value('django.core.mail.backends.smtp.EmailBackend')
    # END EMAIL CONFIGURATION

    # MANAGER CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
    ADMINS = (
        ("""James Lin""", 'james@lin.net.nz'),
    )

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
    MANAGERS = ADMINS
    # END MANAGER CONFIGURATION

    # DATABASE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
    # DATABASES = values.DatabaseURLValue('postgres://localhost/nzpg')
    DATABASES = values.DatabaseURLValue('mysql://root:@localhost/nzpg')
    # END DATABASE CONFIGURATION

    # CACHING
    # Do this here because thanks to django-pylibmc-sasl and pylibmc
    # memcacheify (used on heroku) is painful to install on windows.
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': ''
        }
    }
    # END CACHING

    # GENERAL CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
    TIME_ZONE = 'Pacific/Auckland'

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
    LANGUAGE_CODE = 'en-nz'

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
    SITE_ID = 1

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
    USE_I18N = True

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
    USE_L10N = True

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
    USE_TZ = True
    # END GENERAL CONFIGURATION

    # TEMPLATE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
    TEMPLATE_CONTEXT_PROCESSORS = (
        'django.contrib.auth.context_processors.auth',
        'django.core.context_processors.debug',
        'django.core.context_processors.i18n',
        'django.core.context_processors.media',
        'django.core.context_processors.static',
        'django.core.context_processors.tz',
        'django.contrib.messages.context_processors.messages',
        'django.core.context_processors.request',
        # 'allauth.account.context_processors.account',
        # 'allauth.socialaccount.context_processors.socialaccount',
        'nzpg.apps.comments.context_processors.comment_form',
        'nzpg.apps.contrib.context_processors.common_settings',
        # Your stuff: custom template context processers go here
    )

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
    TEMPLATE_DIRS = (
        join(BASE_DIR, 'templates'),
    )

    TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )

    # See: http://django-crispy-forms.readthedocs.org/en/latest/install.html#template-packs
    CRISPY_TEMPLATE_PACK = 'bootstrap3'
    # END TEMPLATE CONFIGURATION

    # STATIC FILE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
    STATIC_ROOT = join(os.path.dirname(BASE_DIR), 'staticfiles')

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
    STATIC_URL = '/static/'

    # See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
    STATICFILES_DIRS = (
        join(BASE_DIR, 'static'),
    )

    # See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    )
    # END STATIC FILE CONFIGURATION

    # MEDIA CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
    MEDIA_ROOT = join(BASE_DIR, 'media')

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
    MEDIA_URL = '/media/'
    # END MEDIA CONFIGURATION

    # URL Configuration
    ROOT_URLCONF = 'nzpg.urls'

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
    WSGI_APPLICATION = 'nzpg.wsgi.application'
    # End URL Configuration

    # AUTHENTICATION CONFIGURATION
    AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'allauth.account.auth_backends.AuthenticationBackend',
    )

    # Some really nice defaults
    ACCOUNT_EMAIL_REQUIRED = not DEBUG
    ACCOUNT_AUTHENTICATION_METHOD = 'email' if ACCOUNT_EMAIL_REQUIRED else 'username'
    #ACCOUNT_EMAIL_VERIFICATION = 'mandatory' if ACCOUNT_EMAIL_REQUIRED else 'optional'
    ACCOUNT_EMAIL_VERIFICATION = 'none'
    # END AUTHENTICATION CONFIGURATION

    # Custom user app defaults
    # Select the correct user model
    AUTH_USER_MODEL = 'users.User'
    LOGIN_REDIRECT_URL = 'users:redirect'
    LOGIN_URL = 'account_login'
    # END Custom user app defaults

    # SLUGLIFIER
    AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'
    # END SLUGLIFIER

    # LOGGING CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
    # A sample logging configuration. The only tangible logging
    # performed by this configuration is to send an email to
    # the site admins on every HTTP 500 error when DEBUG=False.
    # See http://docs.djangoproject.com/en/dev/topics/logging for
    # more details on how to customize your logging configuration.
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },
        'handlers': {
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler'
            },
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
            },
            'syslog': {
                'level': 'DEBUG',
                'class': 'logging.handlers.SysLogHandler',
                'address': ('logs3.papertrailapp.com', 13291)
            },
        },
        'loggers': {
            'nzpg': {
                'handlers': ['console', 'syslog'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'django.request': {
                'handlers': ['mail_admins', 'console', 'syslog'],
                'level': 'ERROR',
                'propagate': True,
            },
        }
    }
    # END LOGGING CONFIGURATION

    @classmethod
    def post_setup(cls):
        cls.DATABASES['default']['ATOMIC_REQUESTS'] = True

    # Your common stuff: Below this line define 3rd party library settings

    TAGGIT_FORCE_LOWERCASE = True

    SOCIALACCOUNT_PROVIDERS = {
       'facebook':{
            # 'SCOPE': ['email', 'publish_actions', 'user_groups'],
            'SCOPE': ['email', 'publish_actions'],
        }
    }

    # ACCOUNT_ADAPTER = 'nzpg.apps.users.allauth.adapter.NZPGAccountAdapter'

    # SOCIALACCOUNT_AVATAR_SUPPORT = True

    # search
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': os.path.join(dirname(BASE_DIR), 'whoosh_index'),
        },
    }

    HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

    BROKER_URL = values.Value('amqp://guest:guest@localhost:5672//')

    CELERYBEAT_SCHEDULE = {
        'notification-competition-finished': {
            'task': 'nzpg.apps.notifications.tasks.competition_finished',
            'schedule': timedelta(minutes=5),
        },
        'notification-event-tomorrow': {
            'task': 'nzpg.apps.notifications.tasks.event_tomorrow',
            'schedule': timedelta(minutes=5),
        },

    }

    # number of inappropriate reported to deactivate the post
    INAPPROPRIATE_THRESHOLD = values.IntegerValue(20)

    MAX_PHOTO_SIZE = values.IntegerValue(1024000 * 2)

    # PIPELINE
    #STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

    #PIPELINE_COMPILERS = (
        #'pipeline.compilers.sass.SASSCompiler',
    #)

    #PIPELINE_CSS = {
        #'base': {
            #'source_filenames': (
                #'sass/project.scss',
                #'vendor/jquery-ui/themes/base/all.css',
            #),
            #'output_filename': 'css/project.css',
            #'extra_context': {
                #'media': 'screen,projection',
            #},
        #},
    #}


    FORMAT_MODULE_PATH = 'nzpg.formats'

    #REST_FRAMEWORK = {
        #'DEFAULT_PERMISSION_CLASSES': (
            #'rest_framework.permissions.IsAuthenticated',
            #'nzpg.apps.api.permissions.UserIsActive',
        #)
    #}

    GA_ACCOUNT = values.Value('')
    THUMBNAIL_KVSTORE = values.Value('sorl.thumbnail.kvstores.cached_db_kvstore.KVStore')

    # For breaking static cache
    CACHE_BREAKER = '201509181'
