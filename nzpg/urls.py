# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # url(r'^', include('nzpg.apps.contrib.urls', namespace="contrib")),
    url(r'^', include('nzpg.apps.home.urls', namespace="home")),
    url(r'^api/', include('nzpg.apps.api.urls', namespace="api")),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^stream/', include('nzpg.apps.stream.urls', namespace="stream")),
    url(r'^albums/', include('nzpg.apps.photos.urls', namespace="photos")),
    url(r'^governance/', include('nzpg.apps.governance.urls', namespace="governance")),
    url(r'^competitions/', include('nzpg.apps.competitions.urls', namespace="competitions")),
    url(r'^tags/', include('nzpg.apps.tags.urls', namespace="tags")),
    url(r'^likes/', include('nzpg.apps.likes.urls', namespace="likes")),
    url(r'^comments/', include('nzpg.apps.comments.urls', namespace="comments")),
    url(r'^notifications/', include('nzpg.apps.notifications.urls', namespace="notifications")),
    url(r'^search/', include('haystack.urls')),
    url(r'^about/$',
        TemplateView.as_view(template_name='pages/about.html'),
        name="about"),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # User management
    url(r'^users/', include("nzpg.apps.users.urls", namespace="users")),
    url(r'^accounts/', include('allauth.urls')),

    # Uncomment the next line to enable avatars
    url(r'^avatar/', include('avatar.urls')),

    # Your stuff: custom urls go here

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
