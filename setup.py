#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import nzpg
version = nzpg.__version__

setup(
    name='nzpg',
    version=version,
    author="James Lin",
    author_email='james@lin.net.nz',
    packages=[
        'nzpg',
    ],
    include_package_data=True,
    install_requires=[
        'Django>=1.7.4',
    ],
    zip_safe=False,
    scripts=['nzpg/manage.py'],
)
