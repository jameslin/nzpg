#!/bin/sh

echo "drop database nzpg" | mysql -uroot -p
echo "create database nzpg" | mysql -uroot -p
./manage.py migrate
./manage.py loaddata fixtures/users.json
./manage.py loaddata fixtures/facebook.json
./manage.py rebuild_index --noinput
