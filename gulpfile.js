var gulp = require('gulp');
var sass = require('gulp-sass');
var browserify = require('browserify');
var reactify = require('reactify');
var babelify = require("babelify");
var source = require('vinyl-source-stream');
var compass = require('gulp-compass');
var babel = require('gulp-babel');

var paths = {
  js: './nzpg/static/js',
  js_built: './nzpg/static/js_built',
  js_lib: 'app',
  css: './nzpg/static/css',
  sass: './nzpg/static/sass',
  image: './nzpg/static/images',
  font: './nzpg/static/fonts',
}

gulp.task('default', function() {

});

var libjs =  [
    paths.js_lib+'/common/jquery.js',
    paths.js_lib+'/common/isotope.js',
    paths.js_lib+'/common/functions.js',
    paths.js_lib+'/common/alt.js',
    paths.js_lib+'/comments/comment_box.js',
    paths.js_lib+'/comments/comment_box.jsx',
    paths.js_lib+'/likes/stores.js',
    paths.js_lib+'/likes/actions.js',
    paths.js_lib+'/likes/likes.js',
    paths.js_lib+'/likes/likes.jsx',
    paths.js_lib+'/votes/votes.js',
    paths.js_lib+'/votes/votes.jsx',
    paths.js_lib+'/common/controls.js',
    'bootstrap',
    'jquery',
]

gulp.task('libjs', function() {
    browserify({
        require: libjs,
    }).transform(babelify, {global:true}).bundle().pipe(source('lib.js')).pipe(gulp.dest(paths.js_built));
})

gulp.task('projectjs', function() {
    browserify({
        entries: [paths.js+'/project.js'],
    }).external(libjs).bundle().pipe(source('project.js')).pipe(gulp.dest(paths.js_built));
});

gulp.task('competitionsjs', function() {
    browserify({
        entries: [paths.js+'/competitions/competitions.js'],
    }).external(libjs).bundle().pipe(source('competitions.js')).pipe(gulp.dest(paths.js_built+'/competitions'));
});

gulp.task('photosjs', function() {
    browserify({
        entries: [paths.js+'/photos/photos.js'],
    }).external(libjs).bundle().pipe(source('photos.js')).pipe(gulp.dest(paths.js_built+'/photos'));
});


gulp.task('js', ['libjs', 'competitionsjs', 'projectjs', 'photosjs']);

gulp.task('js:watch', function () {
    gulp.watch(paths.js+'/**/*.js', ['js']);
});

gulp.task('sass', function(){
    gulp.src(paths.sass+'/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest(paths.css));
});

gulp.task('sass:watch', function () {
    gulp.watch(paths.sass+'/**/*.scss', ['sass']);
});
